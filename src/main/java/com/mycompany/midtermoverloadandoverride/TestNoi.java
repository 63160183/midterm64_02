/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoverloadandoverride;

/**
 *
 * @author ACER
 */
public class TestNoi {
    public static void main(String[] args) {
        Noi noi = new Noi(0,0,10,11,100);
        System.out.println(noi);
        noi.walking('W'); //น้อยเดินไปข้างหน้า 1 ก้าว
        noi.walking('S'); //น้อยเดินไปข้างหลัง 1 ก้าว
        System.out.println(noi);
        noi.walking('A'); //น้อยเดินไปด้านซ้าย 1 ก้าว
        noi.walking('A', 4); //น้อยเดินไปด้านซ้ายอีก 4 ก้าว
        System.out.println(noi);
        noi.walking(); //น้อยเดินไปด้านซ้ายอีก 1 ก้าว
        System.out.println(noi);
        noi.walking(5); //น้อยเดินไปด้านซ้ายอีก 5 ก้าว
        System.out.println(noi);
        noi.walking('S', 8); //น้อยเดินไปด้านหลังอีก 8 ก้าว
        System.out.println(noi);
        noi.walking(1); //น้อยเดินไปด้านหลังอีก 1 ก้าว
        System.out.println(noi);
    }
}
