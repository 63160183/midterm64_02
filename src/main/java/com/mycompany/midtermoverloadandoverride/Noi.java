/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoverloadandoverride;

/**
 *
 * @author ACER
 */
public class Noi {

    private int a;
    private int b;
    private int na;
    private int nb;
    private int N;
    private char last = ' ';

    public Noi(int a, int b, int na, int nb, int N) {
        this.a = a;
        this.b = b;
        this.na = na;
        this.nb = nb;
        this.N = N;
    }

    public boolean walking(char directionNoi) {
        switch (directionNoi) {
            case 'W': //เดินไปข้างหน้า
                if (!HomeMap(a, b - 1)) {
                    YouAreFarFromHome();
                    return false;
                }
                b = b - 1;
                break;
            case 'A': //เดินไปด้านซ้าย
                if (!HomeMap(a, b + 1)) {
                    YouAreFarFromHome();
                    return false;
                }
                b = b + 1;
                break;
            case 'S': //เดินไปด้านหลัง
                if (!HomeMap(a + 1, b)) {
                    YouAreFarFromHome();
                    return false;
                }
                a = a + 1;
                break;
            case 'D': //เดินไปด้านขวา
                if (!HomeMap(a - 1, b)) {
                    YouAreFarFromHome();
                    return false;
                }
                a = a - 1;
                break;
        }
        last = directionNoi;
        if(inHome()){
            YouAreAtHome();
        }
        return true;
    }

    public boolean walking(char directionNoi, int step) {
        for (int i = 0; i < step; i++) {
            if (!this.walking(directionNoi)) {
                return false;
            }
        }
        return true;
    }

    public boolean walking() {
        return this.walking(last);
    }

    public boolean walking(int step) {
        return this.walking(last, step);
    }

    public boolean HomeMap(int a, int b) {
        if (a >= N || a < 0 || b >= N || b < 0) {
            return false;
        }
        return true;
    }
    public void YouAreFarFromHome(){
        System.out.println("You are far from home!!!");
    }
    public void YouAreAtHome(){
        System.out.println("You are at home.");
    }
    public boolean inHome(){
        if(a == na && b == nb){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Noi (" + this.a + ", " + this.b + ")";
    }
}
